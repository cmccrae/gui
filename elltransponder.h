#ifndef ELLTRANSPONDER_H
#define ELLTRANSPONDER_H

#include <QObject>
#include <QGraphicsEllipseItem>
#include <QGraphicsSceneMouseEvent>
#include <QDebug>
#include <QPointF>

class ellTransponder : public QObject, public QGraphicsEllipseItem
{
    Q_OBJECT
public:
    QString ell_type;
    double frequency;
    double signal;
    int range;
    double minSig;

    explicit ellTransponder(QObject *parent = 0);

signals:
    void ellClicked(QPointF barLocation);

protected:
    virtual void mousePressEvent(QGraphicsSceneMouseEvent *event);  // unused, but release doesn't work if this isn't here!!!!????
    virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
    virtual void hoverEnterEvent(QGraphicsSceneHoverEvent *event);
    virtual void hoverLeaveEvent(QGraphicsSceneHoverEvent *event);
};

#endif // ELLTRANSPONDER_H
