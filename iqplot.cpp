#include "iqplot.h"
#include "ui_iqplot.h"

iqplot::iqplot(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::iqplot)
{
	ui->setupUi(this);

// Initialize the QGraphicsScene and attach to the QGraphicsView
    sceneIQ = new QGraphicsScene(ui->graphicsViewIQ); // IQ plot scene
    sceneIQ->setBackgroundBrush(QBrush(QColor(15,15,15,255),Qt::SolidPattern));
    sceneIQ->setSceneRect(-0.5*ui->graphicsViewIQ->width(),-0.5*ui->graphicsViewIQ->height(),ui->graphicsViewIQ->width(),ui->graphicsViewIQ->height());
    ui->graphicsViewIQ->setScene(sceneIQ);
    ui->graphicsViewIQ->scale(1,-1);
}

iqplot::~iqplot()
{
	if (mytune->thread_function.contains("iqplot")) {
		mytune->thread_function.remove(mytune->thread_function.indexOf("iqplot"));
	}

	delete ui;
}

void iqplot::closeEvent(QCloseEvent *event)
{
	Q_UNUSED(event);
	this->deleteLater();
}

void iqplot::keyPressEvent(QKeyEvent *event)
{
	if (event->key() == Qt::Key_Escape) {
		this->close();
	}
}

void iqplot::init()
{
	this->setWindowTitle("IQ Adapter " + QString::number(mytune->adapter) + ", Frontend " + QString::number(mytune->frontend) + " : " + mytune->name);

	connect(mytune, SIGNAL(iqdraw(QVector<short int>, QVector<short int>)), this, SLOT(iqdraw(QVector<short int>, QVector<short int>)));

    if (mytune->caps & FE_CAN_8VSB) {
        ui->comboBox_mode->setEnabled(false);
        ui->comboBox_point->setEnabled(false);
    } else {
        ui->comboBox_mode->setEnabled(true);
        ui->comboBox_point->setEnabled(true);
    }
//	modcod_name.append("QPSK 1/4");
//	modcod_name.append("QPSK 1/3");
//	modcod_name.append("QPSK 2/5");
//	modcod_name.append("QPSK 1/2");
//	modcod_name.append("QPSK 3/5");
//	modcod_name.append("QPSK 2/3");
//	modcod_name.append("QPSK 3/4");
//	modcod_name.append("QPSK 4/5");
//	modcod_name.append("QPSK 5/6");
//	modcod_name.append("QPSK 8/9");
//	modcod_name.append("QPSK 9/10");
//	modcod_name.append("8PSK 3/5");
//	modcod_name.append("8PSK 2/3");
//	modcod_name.append("8PSK 3/4");
//	modcod_name.append("8PSK 5/6");
//	modcod_name.append("8PSK 8/9");
//	modcod_name.append("8PSK 9/10");
//	modcod_name.append("16PSK 2/3");
//	modcod_name.append("16PSK 3/4");
//	modcod_name.append("16PSK 4/5");
//	modcod_name.append("16PSK 5/6");
//	modcod_name.append("16PSK 8/9");
//	modcod_name.append("16PSK 9/10");
//	modcod_name.append("32PSK 3/4");
//	modcod_name.append("32PSK 4/5");
//	modcod_name.append("32PSK 5/6");
//	modcod_name.append("32PSK 8/9");
//	modcod_name.append("32PSK 9/10");

//	int x = -119;
//	for(int i = 0; i < modcod_name.size(); i++) {
//		modcod_marker.append(new QwtPlotMarker);
//		modcod_marker.last()->setLabel(modcod_name.at(i));
//		modcod_marker.last()->setLabelAlignment(Qt::AlignCenter|Qt::AlignBottom);
//		modcod_marker.last()->setLabelOrientation(Qt::Vertical);
//		modcod_marker.last()->setLineStyle(QwtPlotMarker::VLine);
//		modcod_marker.last()->setLinePen(Qt::blue,0,Qt::DotLine);
//		modcod_marker.last()->setValue(x,0);
//		x += 8;
//	}

	mytune->start();
	on_pushButton_onoff_clicked();
}

void iqplot::delete_iqplot()
{
	this->deleteLater();
}

void iqplot::iqdraw(QVector<short int> x, QVector<short int> y)
{
    ui->graphicsViewIQ->scene()->clear();    

    int iSceneWidth = ui->graphicsViewIQ->width();
    int iSceneHeight = ui->graphicsViewIQ->height();

    int count = 0;
    int xy[255][255]{};

    for (int i = 0; i < x.size(); i++){
        xy[x[i] + 127][y[i] + 127]++;
        if (xy[x[i] + 127][y[i] + 127] > count){ count++; }
    }

    qreal barSize = iSceneHeight/255.0;

    for (int j = 0; j < 255; j++){
        for (int k = 0; k < 255; k++){
            if (xy[j][k] > 0){
                addGraphBar(j - 127, k - 127, xy[j][k], barSize);
            }
        }
    }

    QRectF rectIQ = sceneIQ->itemsBoundingRect(); // space occupied by all data points
    qreal maxIQ = 0;  // determine the furthest extent of the data points
    if (abs(rectIQ.bottom()) > maxIQ) {maxIQ = rectIQ.bottom();}
    if (abs(rectIQ.top()) > maxIQ) {maxIQ = rectIQ.top();}
    if (abs(rectIQ.left()) > maxIQ) {maxIQ = rectIQ.left();}
    if (abs(rectIQ.right()) > maxIQ) {maxIQ = rectIQ.right();}

    maxIQ = maxIQ*1.1; // give some extra space around

    rectIQ.setBottom(maxIQ); // make it square
    rectIQ.setTop(-1*maxIQ);
    rectIQ.setLeft(-1*maxIQ);
    rectIQ.setRight(maxIQ);

    ui->graphicsViewIQ->fitInView(rectIQ, Qt::KeepAspectRatio);

    addGraphLine(-0.5*iSceneWidth,0,0.5*iSceneWidth,0);  // x-axis
    addGraphLine(0,-0.5*iSceneHeight,0,0.5*iSceneHeight); // y-axis

    addGraphText(QFont("Monospace", 4),"0",0,0,0,5,"");

    for (int t = -120; t < 128; t = t + 10){
        if (t != 0){
            addGraphLine(t,0,t,-5); // x-axis markers
            addGraphText(QFont("Monospace", 2),QString::number(t),t,-1,0,4,"");
            addGraphLine(-5,t,0,t); // y-axis markers
            addGraphText(QFont("Monospace", 2),QString::number(t),-1,t,0,8,"");
        }
    }
}

void iqplot::on_pushButton_onoff_clicked()
{
	if (mytune->thread_function.contains("iqplot")) {
		mytune->thread_function.remove(mytune->thread_function.indexOf("iqplot"));
		ui->pushButton_onoff->setText("Start");
	} else {
		erase();
		mytune->thread_function.append("iqplot");
		ui->pushButton_onoff->setText("Stop");
	}
}

void iqplot::erase()
{
	mytune->iq_options = ui->comboBox_mode->currentIndex() << 4 | ui->comboBox_point->currentIndex();

	sleep(1);

	mytune->iq_x.clear();
	mytune->iq_y.clear();

//	if (ui->comboBox_point->currentIndex() == 14)
//	{
//		ui->qwtPlot->setMinimumWidth(680);
//		scaleY->detach();
//		for (int i = 0; i < modcod_marker.size(); i++) {
//			modcod_marker.at(i)->attach(ui->qwtPlot);
//		}
//	} else {
//		ui->qwtPlot->setMinimumWidth(384);
//		ui->iqplot_widget->resize(ui->iqplot_widget->minimumWidth(), ui->iqplot_widget->height());
//		scaleY->attach(ui->qwtPlot);
//		for (int i = 0; i < modcod_marker.size(); i++) {
//			modcod_marker.at(i)->detach();
//		}
//	}
}

void iqplot::on_comboBox_mode_currentIndexChanged(int index)
{
	Q_UNUSED(index);
	erase();
}

void iqplot::on_comboBox_point_currentIndexChanged(int index)
{
	Q_UNUSED(index);
	erase();
}

void iqplot::addGraphText(QFont font, QString graphText, qreal x, qreal y, qreal rotate, int origin, QString strObjName)
{
    QGraphicsTextItem *txt = new QGraphicsTextItem;
    txt->setParent(ui->graphicsViewIQ->scene());
    txt->setFont(font);
    txt->setDefaultTextColor(Qt::black);
    txt->setTransform(QTransform().scale(1,-1));
    txt->setRotation(rotate);
    txt->setPlainText(graphText);
    txt->setObjectName(strObjName);

    switch(origin){ // bounding rectangle origin is visualized as top left, this transforms the original x,y to effectively change the placement location
    case 1:     // don't change a thing (top-left)
        break;
    case 2:     // origin is mid-left side
        y = y + 0.5*txt->boundingRect().height(); break;
    case 3:     // origin is bottom-left
        y = y + txt->boundingRect().height(); break;
    case 4:     // origin is top-middle
        x = x - 0.5*txt->boundingRect().width(); break;
    case 5:     // origin is middle-middle
        x = x - 0.5*txt->boundingRect().width();
        y = y + 0.5*txt->boundingRect().height(); break;
    case 6:     // origin is bottom-middle
        x = x - 0.5*txt->boundingRect().width();
        y = y + txt->boundingRect().height(); break;
    case 7:     // origin is top-right
        x = x - txt->boundingRect().width(); break;
    case 8:     // origin is middle-right
        x = x - txt->boundingRect().width();
        y = y + 0.5*txt->boundingRect().height(); break;
    case 9:     // origin is bottom-right
        x = x - txt->boundingRect().width();
        y = y + txt->boundingRect().height(); break;
    default:
        qDebug() << "Invalid text origin transformation specified";
        break;
    }
    txt->setPos(x,y);

    sceneIQ->addItem(txt);
}

void iqplot::addGraphBar(int x, int y, int count, double dBarWidth)
{
    int iShade = 50 + 5*count;  // rough estimate of how to scale the hit count with a minimum level
    if (iShade > 255){iShade=255;}  // just in case the estimate exceeds the RGB limit
    QGraphicsRectItem *bar = new QGraphicsRectItem;
    bar->setBrush(QBrush(QColor(0,iShade,0,255),Qt::SolidPattern));
    bar->setPen(QPen(QColor(0,0,0,0)));
    bar->setRect(x,y,dBarWidth,dBarWidth);
    sceneIQ->addItem(bar);
}

void iqplot::addGraphLine(qreal x1, qreal y1, qreal x2, qreal y2)
{
    QGraphicsLineItem *newLine = new QGraphicsLineItem();
    newLine->setLine(x1, y1, x2, y2);
    newLine->setPen(QPen(Qt::black,0));
    sceneIQ->addItem(newLine);
}
