#ifndef IQPLOT_H
#define IQPLOT_H

#include <QWidget>
#include <QKeyEvent>
#include <QGraphicsScene>
#include "dvbtune.h"
#include <barsignal.h>

namespace Ui {
class iqplot;
}

class iqplot : public QWidget
{
	Q_OBJECT

public:
	explicit iqplot(QWidget *parent = 0);
	~iqplot();

	void init();
	void erase();

	dvbtune *mytune;

private slots:
	void delete_iqplot();
	void iqdraw(QVector<short int> x, QVector<short int> y);
	void on_pushButton_onoff_clicked();
	void on_comboBox_mode_currentIndexChanged(int index);
	void on_comboBox_point_currentIndexChanged(int index);

private:
	Ui::iqplot *ui;
//	QVector<QString> modcod_name;
//	QVector<QwtPlotMarker *> modcod_marker;
    QGraphicsScene *sceneIQ;

    void addGraphText(QFont font, QString graphText, qreal x, qreal y, qreal rotate, int origin, QString strObjName);
    void addGraphBar(int x, int y, int count, double dBarWidth);
    void addGraphLine(qreal x1, qreal y1, qreal x2, qreal y2);

protected:
	void closeEvent(QCloseEvent *event);
	void keyPressEvent(QKeyEvent *event);
};

#endif // IQPLOT_H
