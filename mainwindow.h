/*	
 *	updateDVB, a DVB/ATSC spectrum analyzer, tuning and stream analyzing application.
 *	Copyright (C) 2013  Chris Lee (updatelee@gmail.com)
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *	
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QVector>
#include <QSettings>
#include <QString>
#include <QTimer>
#include <QGraphicsScene>
#include <QGraphicsRectItem>
#include "dvb_settings.h"
#include "scan.h"
#include "settings.h"
#include "blindscan.h"
#include <barsignal.h>
#include <elltransponder.h>

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void scanDataReceived(QVector<double> x, QVector<double> y, int min, int max, int cindex, unsigned int scale);
    void tuneScan(double frequency);
	void update_status(QString text, int time = STATUS_REMOVE);

private slots:
	void on_pushButton_spectrumscan_clicked();
	void on_pushButton_blindscan_clicked();
	void on_pushButton_usals_go_clicked();
	void on_pushButton_gotox_go_clicked();
	void on_pushButton_gotox_save_clicked();
	void on_pushButton_drive_east_L_clicked();
	void on_pushButton_drive_east_S_clicked();
	void on_pushButton_drive_west_S_clicked();
	void on_pushButton_drive_west_L_clicked();
	void on_comboBox_frontend_currentIndexChanged(int index);
	void on_comboBox_lnb_currentIndexChanged(int index);
	void on_comboBox_voltage_currentIndexChanged(int index);
	void on_lineEdit_usals_returnPressed();
	void on_checkBox_loop_stateChanged();
	void on_checkBox_waterfall_clicked();
	void on_actionSettings_triggered();
	void on_actionExit_triggered();
	void adapter_status(int adapter);
	void setup_tuning_options();
	void on_actionSave_Screenshot_triggered();
	void on_comboBox_adapter_activated(int index);

	void on_comboBox_modcod_currentIndexChanged(int index);

    void slotBarClicked(QPointF barLocation);
    void slotEllClicked(QPointF ellLocation);

private:
	Ui::MainWindow *ui;
	QVector< QPointer<tuning> > mytuning;
    scan *myscan;
	QVector< QVector<double> > waterfall_x_V;
	QVector< QVector<double> > waterfall_x_H;
	QVector< QVector<double> > waterfall_x_N;
	QVector< QVector<double> > waterfall_y_V;
	QVector< QVector<double> > waterfall_y_H;
	QVector< QVector<double> > waterfall_y_N;
	QSettings *mysettings;
	QVector<tuning_options> tune_ops;
	dvb_settings dvbnames;
	QVector<dvbtune*> mytuners;
	QVector<blindscan*> myblindscan;
	bool noload;
	QVector<QString> mystatus;
	QSignalMapper status_mapper;
	QTimer *status_timer;
	QVector<QString> ss_filename;
    QGraphicsScene *scene;

	void getadapters();
	void reload_settings();
	void add_comboBox_modulation(QString name);
    void addGraphText(QFont font, QString graphText, qreal x, qreal y, qreal rotate, int origin, QString strObjName);
    void addGraphBorder(QPen pen, qreal border, qreal width, qreal height);
    void addGraphBar(QString type, double freq, double signal, double dBarWidth, int iMargin, double minFreq, double maxSig, double minSig);    
    void addGraphHighlightBar(QString type, double freq, double signal, double dBarWidth, int iMargin, double minFreq, double minSig);

protected:
	void closeEvent(QCloseEvent *event);
	void focusInEvent(QFocusEvent * event);
    void resizeEvent(QResizeEvent *event);
};

#endif // MAINWINDOW_H
