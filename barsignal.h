#ifndef BARSIGNAL_H
#define BARSIGNAL_H

#include <QObject>
#include <QGraphicsRectItem>
#include <QGraphicsSceneMouseEvent>
#include <QPoint>
#include <QPointF>
#include <QPen>
#include <QToolTip>
#include <QEvent>

class barSignal : public QObject , public QGraphicsRectItem
{
    Q_OBJECT
public:
    QString bar_type;
    double frequency;
    double frequency_range;
    double minFreq;
    double signal;
    int range;
    double minSig;
    int channel;
    qreal orig_x, orig_y, orig_width, orig_height, highlight_width;

    explicit barSignal(QObject *parent = 0);

signals:
    void barClicked(QPointF barLocation);
    void barHovered(QPointF barLocation);

protected:
    virtual void mousePressEvent(QGraphicsSceneMouseEvent *event);  // unused, but release doesn't work if this isn't here!!!!????
    virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
    virtual void hoverEnterEvent(QGraphicsSceneHoverEvent *event);
    virtual void hoverLeaveEvent(QGraphicsSceneHoverEvent *event);
    virtual void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    virtual void hoverMoveEvent(QGraphicsSceneHoverEvent *event);

private:
    void setBarPos(qreal event_x);
};

#endif // BARSIGNAL_H
