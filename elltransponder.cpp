#include "elltransponder.h"

ellTransponder::ellTransponder(QObject *parent) : QObject(parent)
{
    setAcceptHoverEvents(true);
}

void ellTransponder::mousePressEvent(QGraphicsSceneMouseEvent *event) // Release doesn't seem to work without press being present
{
    Q_UNUSED(event);
}

void ellTransponder::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    if (event->button() == Qt::LeftButton){
        emit ellClicked(event->scenePos());
    }
}

void ellTransponder::hoverEnterEvent(QGraphicsSceneHoverEvent *event)
{
    Q_UNUSED(event);
    setOpacity(0.8);
    setTransformOriginPoint(rect().x() + boundingRect().width()/2,rect().y() + boundingRect().height()/2);
    setScale(3);
}

void ellTransponder::hoverLeaveEvent(QGraphicsSceneHoverEvent *event)
{
    Q_UNUSED(event);
    setOpacity(1);
    setTransformOriginPoint(rect().x() + boundingRect().width()/2,rect().y() + boundingRect().height()/2);
    setScale(1);
}

