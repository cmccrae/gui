#include "barsignal.h"

barSignal::barSignal(QObject *parent) : QObject(parent)
{
    setAcceptHoverEvents(true);
    highlight_width = 6; // specify the width in MHz
    setBrush(QBrush(QColor(0,0,0,255),Qt::SolidPattern));
    setZValue(0);
}

void barSignal::mousePressEvent(QGraphicsSceneMouseEvent *event) // Release doesn't seem to work without press being present
{
    Q_UNUSED(event);
}

void barSignal::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    if (event->button() == Qt::LeftButton){
        emit barClicked(event->scenePos());
    }
}

void barSignal::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    if (bar_type == "highlight"){
        setBarPos(event->scenePos().x());
        frequency = minFreq + ((event->scenePos().x() - orig_x)/orig_width)*frequency_range;
        QToolTip::showText(QPoint(event->screenPos().x(),event->screenPos().y()), QString::number(frequency));
        setToolTip(QString::number(frequency));
    }
}

void barSignal::hoverMoveEvent(QGraphicsSceneHoverEvent *event)
{
    if (bar_type == "highlight"){
        setBarPos(event->scenePos().x());
        frequency = (int)(minFreq + ((event->scenePos().x() - orig_x)/orig_width)*frequency_range);
        QToolTip::showText(QPoint(event->screenPos().x(),event->screenPos().y()), QString::number(frequency));
        setToolTip(QString::number(frequency));
    }
}

void barSignal::hoverEnterEvent(QGraphicsSceneHoverEvent *event)
{
    if (bar_type != "highlight"){
        setOpacity(0.8);
    }
    else {
        setBrush(QBrush(QColor(200,200,200,255),Qt::SolidPattern));
        setOpacity(0.2);
        setBarPos(event->scenePos().x());
    }
}

void barSignal::hoverLeaveEvent(QGraphicsSceneHoverEvent *event)
{
    Q_UNUSED(event);

    if (bar_type != "highlight"){
        setOpacity(1);
    }
    else {
        setBrush(QBrush(QColor(0,0,0,255),Qt::SolidPattern));
        setOpacity(0.001);
        setRect(orig_x, orig_y, orig_width, orig_height);
    }
}

void barSignal::setBarPos(qreal event_x)
{
    qreal screenHighlight = orig_width * highlight_width / frequency_range; // determine screen size of highlight_width (MHz) ... assumes frequency range is in MHz, not kHz (ATSC)

    if ((event_x - screenHighlight/2) < orig_x){ // stay above lower bound
        setRect(orig_x, orig_y, screenHighlight, orig_height);
    }
    else if ((event_x + screenHighlight/2) > (orig_x + orig_width)){ // stay below upper bound
        setRect(orig_x + orig_width - screenHighlight, orig_y, screenHighlight, orig_height);
    }
    else { // everywhere else in between
        setRect(event_x - screenHighlight/2, orig_y, screenHighlight, orig_height);
    }
}
